package com.generator;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import com.generator.lib.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Path daysPath = Path.of(Settings.DAYS_PATH);
        Path monthPath = Path.of(Settings.MONTHS_PATH);
        Path yearPath = Path.of(Settings.YEARS_PATH);
        Path dividersPath = Path.of(Settings.DIVIDERS_PATH);
        ResultToFile resultToFile = new ResultToFile(Settings.SAVE_TO_FILE);
        List<ArrayList> days = ReadFile.datesList(daysPath);
        List<ArrayList> months = ReadFile.datesList(monthPath);
        List<ArrayList> years = ReadFile.datesList(yearPath);
        List<String> dividers = ReadFile.dividersList(dividersPath);
        int i = 0;
        StringBuilder sb = new StringBuilder();
        String debugDate = "";
        for (ArrayList<String> day : days){

            System.out.println("progress: ".concat(String.format ("%,.0f",((double)(i++) / 31)*100)).concat("\t%"));
            for (String d : day){
                for (ArrayList<String> month : months){
                    for (String m : month){
                        for (ArrayList<String> year : years){
                            for (String y : year){
                                for (String div : dividers){
                                    debugDate = getDateDebug(day.get(0), month.get(0), year.get(0));
                                    sb.append(getDateString(d, m, y, div)).append(debugDate);
                                    sb.append(getDateString(m, d, y, div)).append(debugDate);
                                    sb.append(getDateString(y, m, d, div)).append(debugDate);
                                    sb.append(getDateString(y, d, m, div)).append(debugDate);
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Saving...");
        //System.out.println(sb.toString());
        resultToFile.write(sb.toString());
        System.out.println("Done");
    }

    private static String getDateString(String date1, String date2, String date3, String div){
        return date1.concat(div).concat(date2).concat(div).concat(date3);
    }

    public static String getDateDebug(String day, String month, String year){
        try {
            int dayI = Integer.parseInt(day);
            int monthI = Integer.parseInt(month);
            int yearI = Integer.parseInt(year);
            return String.format("\t\tDayOfMonth=%d|Month=%d|Years=%d\n", dayI, monthI,yearI);
        }
        catch (Exception e){
            return "error in data \"" + day + "-" + month + "-" + year + "\"";
        }
    }
}
