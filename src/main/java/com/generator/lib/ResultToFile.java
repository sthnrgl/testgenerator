package com.generator.lib;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ResultToFile {
    private String pathToDirectory;
    public ResultToFile(){
        pathToDirectory = "strings/";
    }
    public ResultToFile(String pathToDirectory){
        this.pathToDirectory = pathToDirectory;
    }
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd__HH-mm-ss");
    private  LocalDateTime now = LocalDateTime.now();

    public void write(String s) throws IOException {
        Path path = Path.of(pathToDirectory + "result_" + dtf.format(now) + ".txt");
        Files.createFile(path);
        Files.write(path, s.getBytes());
        System.out.println(path.getFileName() + "\t\t" + Files.size(path) + " b");
    }
}
