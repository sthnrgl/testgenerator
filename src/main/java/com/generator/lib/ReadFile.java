package com.generator.lib;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    public static List<ArrayList> datesList(Path path) throws IOException {
        try {
            List<String> data = Files.readAllLines(path);
            List<ArrayList> lists = new ArrayList<>();
            for (String s : data) {
                ArrayList coll = new ArrayList();
                coll.addAll(List.of(s.split(Settings.SPLIT_IN_FILES)));
                lists.add(coll);

            }
            return lists;

        } catch (IOException e) {
            System.out.println("Error in file \"" + path.toString() + "\"");
            return null;
        }
    }
    public static List<String> dividersList(Path dividersPath){
        try {
            List<String> data = Files.readAllLines(dividersPath);
            data.add(" ");
            return data;

        }
        catch(IOException e){
            System.out.println("Error in file \"" + dividersPath.toString() + "\"");
            return null;
        }
    }
}
