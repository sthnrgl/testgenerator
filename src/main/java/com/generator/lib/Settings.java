package com.generator.lib;

public class Settings {
    public static final String SPLIT_IN_FILES = ", ";
    public static final String SAVE_TO_FILE = "result/";
    public static final String DIVIDERS_PATH = "strings/dividers.txt";
    public static final String DAYS_PATH = "strings/days.txt";
    public static final String MONTHS_PATH = "strings/months.txt";
    public static final String YEARS_PATH = "strings/years.txt";

    public static final int START_YEAR = 1980;
    public static final int END_YEAR  = 2023;
}
